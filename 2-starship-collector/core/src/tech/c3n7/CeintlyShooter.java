package tech.c3n7;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.ScreenUtils;

public class CeintlyShooter extends ApplicationAdapter {
	SpriteBatch batch;
	Color clearColor;

	Texture playerShip;
	Rectangle playerRect;
	float playerX, playerY;

	Texture coin;
	Rectangle coinRect;
	float coinX, coinY;

	boolean win;
	Texture winTexture;

	@Override
	public void create() {
		clearColor = Color.valueOf("#202125");
		batch = new SpriteBatch();

		playerShip = new Texture("simplespace/station_C.png");
		playerX = 20;
		playerY = 20;
		playerRect = new Rectangle(playerX, playerY, playerShip.getWidth(), playerShip.getHeight());

		coin = new Texture("simplespace/meteor_squareSmall.png");
		coinX = 380;
		coinY = 380;
		coinRect = new Rectangle(coinX, coinY, coin.getWidth(), coin.getHeight());

		win = false;
		winTexture = new Texture("simplespace/icon_plusLarge.png");
	}

	@Override
	public void render() {
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			playerX--;
		}
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			playerX++;
		}
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			playerY++;
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			playerY--;
		}

		playerRect.setPosition(playerX, playerY);

		// check win condition
		if (playerRect.overlaps(coinRect)) {
			win = true;
		}

		ScreenUtils.clear(clearColor.r, clearColor.g, clearColor.b, 1);
		batch.begin();

		if (!win) {
			batch.draw(coin, coinX, coinY);
		}
		batch.draw(playerShip, playerX, playerY);
		if (win) {
			batch.draw(winTexture, 180, 180);
		}

		batch.end();
	}

	@Override
	public void dispose() {
		batch.dispose();
		playerShip.dispose();
	}
}
